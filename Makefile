PREFIX=`opam config var prefix`/share/bap

all: build install


.PHONY: build
build :
	make -C ddtbd

.PHONY: install
install :
	make -C ddtbd install
	install -d $(PREFIX)/spectre.recipe
	install -m 664 check/* $(PREFIX)/spectre.recipe

.PHONY: clean
clean :
	make -C ddtbd clean

.PHONY: uninstall

uninstall :
	rm -rf $(PREFIX)/spectre.recipe
	make -C ddtbd uninstall
